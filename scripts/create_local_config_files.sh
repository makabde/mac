#!/bin/sh

cd "$(dirname "$0}")" &&
    . "utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

create_local_gitconfig() {

    LOCAL_GITCONFIG_PATH="$HOME/.gitconfig.local"

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    if [ ! -e "$LOCAL_GITCONFIG_PATH" ] || [ -z "$LOCAL_GITCONFIG_PATH" ]; then

        printf "%s\n" "
[commit]

    # Sign commits using GPG.
    # https://help.github.com/articles/signing-commits-using-gpg/

    # gpgsign = true

[user]

    name =
    email =
    # signingkey =" >>"$LOCAL_GITCONFIG_PATH"

    fi

    print_result $? "$LOCAL_GITCONFIG_PATH"

}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

create_local_zshrc() {

    LOCAL_ZSHRC_PATH="$HOME/.zshrc.local"

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    if [ ! -e "$LOCAL_ZSHRC_PATH" ] || [ -z "$LOCAL_ZSHRC_PATH" ]; then

        touch "$LOCAL_ZSHRC_PATH"

    fi

    print_result $? "$LOCAL_ZSHRC_PATH"

}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

main() {

    print_in_purple "\n • Creating local config files\n\n"

    create_local_gitconfig
    create_local_zshrc

}

main
