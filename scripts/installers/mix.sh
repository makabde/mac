#!/bin/sh

cd "$(dirname "$0")" &&
    . "../utils.sh" &&
    . "utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

install_phoenix() {

    printf "\n" | mix archive.install hex phx_new &>/dev/null
    #  └─ simulate the ENTER keypres
    print_result $? "Phoenix"

}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

main() {

    print_in_purple "\n • Mix (Elixir)\n\n"

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    mix_install "hex (install)" "local.hex --force"
    mix_install "rebar (install)" "local.rebar --force"

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    printf "\n"

    execute "install_phoenix" "phoenix (install)"

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    asdf reshim elixir

}

main
