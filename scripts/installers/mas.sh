#!/bin/sh

cd "$(dirname "$0")" &&
    . "../utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

print_in_purple "\n\tmacOS Apps\n"

brew bundle --file=- <<EOF
tap "homebrew/bundle"

mas "1Password", id: 443987910
mas "GarageBand", id: 682658836
mas "iMovie", id: 408981434
mas "Keynote", id: 409183694
mas "Numbers", id: 409203825
mas "Pages", id: 409201541
mas "Pocket", id: 568494494
mas "Save to Pocket", id: 1477385213
mas "Xcode", id: 497799835
EOF
