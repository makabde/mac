#!/bin/sh

cd "$(dirname "$0")" &&
    . "../utils.sh" &&
    . "utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

change_default_zsh() {

    newShellPath=""
    brewPrefix=""

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    # Try to get the path of the `Zsh` version installed through
    # `Homebew`.

    brewPrefix="$(brew_prefix)" ||
        return 1

    newShellPath="$brewPrefix/bin/zsh"

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    # Add the path of the `Zsh` version installed through `Homebrew`
    # to the list of login shells from the `/etc/shells` files.
    #
    # This needs to be done because applications use this file to
    # determine whether a shell is valid (e.g.: `chsh` consults the
    # `/etc/shells` to determine whether an unprivileged user may
    # change the login shell for her own account).
    #
    # http://www.linuxfromscratch.org/blfs/view/7.4/postlfs/etcshells.html

    if ! grep "$newShellPath" </etc/shells &>/dev/null; then

        execute \
            "printf '%s\n' '$newShellPath' | sudo tee -a /etc/shells" \
            "Zsh (add '$newShellPath' in '/etc/shells')" ||
            return 1

    fi

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    # Set the latest version of `Zsh` as the default (macOs uses
    # by default an older version of `Zsh`).

    chsh -s "$newShellPath" "$USER" &>/dev/null
    print_result $? "Zsh (use latest version)"

}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

main() {

    print_in_purple "\n\tZSH\n\n"

    case "$SHELL" in

    */zsh)
        # TODO: refactor this code to remove the hardcoded reference to
        # the installed zsh instance.
        if [ "$(command -v zsh)" != '/usr/local/bin/zsh' ]; then

            change_default_zsh

        else

            print_success "Zsh (use latest version)"

        fi
        ;;
    *)
        change_default_zsh
        ;;

    esac

}

main
