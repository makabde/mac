#!/bin/sh

cd "$(dirname "$0")" &&
    . "../utils.sh" &&
    . "utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

install_or_update_node() {

    PLUGIN_NAME="nodejs"
    PLUGIN_URL="https://github.com/asdf-vm/asdf-nodejs.git"

    asdf_install_or_update_plugin $PLUGIN_NAME $PLUGIN_URL

    asdf_install_language $PLUGIN_NAME
}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

main() {

    print_in_purple "\n\tNodeJS\n\n"

    install_or_update_node

}

main
