#!/bin/sh

cd "$(dirname "$0")" &&
    . "../utils.sh" &&
    . "utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

install_or_update_erlang() {

    PLUGIN_NAME="erlang"
    PLUGIN_URL="https://github.com/asdf-vm/asdf-erlang.git"

    export KERL_CONFIGURE_OPTIONS="--disable-debug --without-javac"

    asdf_install_or_update_plugin $PLUGIN_NAME $PLUGIN_URL
    asdf_install_language $PLUGIN_NAME

}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

main() {

    print_in_purple "\n\tERLANG\n\n"

    install_or_update_erlang

}

main
