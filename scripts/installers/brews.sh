#!/bin/sh

cd "$(dirname "$0")" &&
    . "../utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

print_in_purple "\n\tBrew's\n"

brew bundle --file=- <<EOF
tap "homebrew/bundle"

# Build Dependencies
brew "coreutils"
brew "automake"
brew "openssl"
brew "libyaml" # Must come after openssl
brew "readline"
brew "libxslt"
brew "libtool"
brew "unixodbc"
brew "unzip"
brew "autoconf"
brew "curl"
brew "wxwidgets"

# Shell
brew "zsh"
brew "spaceship"
brew "terminal-notifier"
brew "mackup"

# ASDF
brew "asdf"

# Tmux
brew "tmux"
brew "reattach-to-user-namespace"

# Development CLI
brew "git"
brew "shellcheck"
brew "watchman"

# NodeJS
# brew "yarn", args: ["without-node"]

# Browsers
# Firefox
cask "google-chrome"
cask "google-chrome@canary"
cask "chromium"
# Firefox
cask "firefox"
cask "firefox@developer-edition"
cask "firefox@nightly"
# Safari
cask "safari-technology-preview"

# Design
cask "sketch"
cask "figma"
cask "skyfonts"

# Development
cask "dash"
cask "docker"
cask "insomnia"
cask "iterm2"
cask "visual-studio-code"

# Productivity
# cask "alfred"
cask "appcleaner"
cask "alt-tab"
cask "bartender"
cask "choosy"
cask "raycast"
cask "rectangle"

# Slacking
cask "discord"
cask "slack"

# Security
cask "authy"
cask "gpg-suite@nightly"
cask "protonvpn"
cask "protonmail-bridge"

# System Monitoring
cask "stats"

# Trading
cask "tradingview"

# Writing
cask "notion"

# Fonts
cask "font-anonymous-pro"
cask "font-fira-code"
cask "font-hack"
EOF
