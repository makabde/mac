#!/bin/sh

cd "$(dirname "${BASH_SOURCE[0]}")" &&
    . "../utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

print_in_purple "\n • Installing CLI & GUI tools\n\n"

./xcode.sh

./homebrew.sh
./brews.sh

./zsh.sh

./erlang.sh
./elixir.sh
./mix.sh

./node.sh
./npm.sh

./tmux.sh

./mas.sh
