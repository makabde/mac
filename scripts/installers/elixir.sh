#!/bin/sh

cd "$(dirname "$0")" &&
    . "../utils.sh" &&
    . "utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

install_or_update_elixir() {

    PLUGIN_NAME="elixir"
    PLUGIN_URL="https://github.com/asdf-vm/asdf-elixir.git"

    asdf_install_or_update_plugin $PLUGIN_NAME $PLUGIN_URL
    asdf_install_language $PLUGIN_NAME
}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

main() {

    print_in_purple "\n\tElixir\n\n"

    install_or_update_elixir

}

main
