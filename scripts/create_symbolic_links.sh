#!/bin/sh

cd "$(dirname "$0")" && . "utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

create_symbolic_links() {

    FILES_TO_SYMLINK=(
        \
        "shell/aliases"
        "shell/curlrc"
        "shell/hushlogin"
        "shell/inputrc"
        "shell/nv"
        "shell/tmux.conf"
        "shell/zsh"
        "shell/zprofile"
        "shell/zshrc"
        \
        "git/gitattributes"
        "git/gitconfig"
        "git/gitignore"
        \
        "mackup/mackup.cfg"
    )

    i=""
    sourceFile=""
    targetFile=""
    _skip_questions=false

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    skip_questions "$@" &&
        _skip_questions=true

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    for i in "${FILES_TO_SYMLINK[@]}"; do

        sourceFile="$(cd ../dotfiles && pwd)/$i"
        targetFile="$HOME/.$(printf "%s" "$i" | sed "s/.*\/\(.*\)/\1/g")"

        if [ ! -e "$targetFile" ] || $_skip_questions; then

            execute \
                "ln -fs $sourceFile $targetFile" \
                "$targetFile → $sourceFile"

        elif [ "$(readlink "$targetFile")" == "$sourceFile" ]; then

            print_success "$targetFile → $sourceFile"

        else

            if ! $_skip_questions; then

                ask_for_confirmation "'$targetFile' already exists, do you want to overwrite it?"
                if answer_is_yes; then

                    rm -rf "$targetFile"

                    execute \
                        "ln -fs $sourceFile $targetFile" \
                        "$targetFile → $sourceFile"

                else
                    print_error "$targetFile → $sourceFile"
                fi

            fi

        fi

    done

}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

main() {

    print_in_purple "\n • Creating symbolic links\n\n"
    create_symbolic_links "$@"

}

main "$@"
