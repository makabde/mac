#!/bin/sh

cd "$(dirname "$0")" && . "utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

create_directories() {

    DIRECTORIES=(
        "$HOME/.bin"
        "$HOME/Github"
        "$HOME/.tmux"
    )

    for i in "${DIRECTORIES[@]}"; do
        mkd "$i"
    done

}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

main() {

    print_in_purple "\n • Creating directories\n\n"
    create_directories

}

main
