# Squirrelbook Zsh prompt using Spaceship
# https://github.com/denysdovhan/spaceship-prompt

# Hide VI mode
SPACESHIP_VI_MODE_SHOW=false
