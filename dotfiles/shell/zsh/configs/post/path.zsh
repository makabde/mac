# ensure dotfiles bin directory is loaded first
PATH="$HOME/.bin:/usr/local/bin:$PATH"



# mkdir .git/safe in the root of repositories you trust
PATH=".git/safe/../../bin:$PATH"

export PATH="${ASDF_DATA_DIR:-$HOME/.asdf}/shims:$PATH"
export -U PATH
